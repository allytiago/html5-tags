# Head

A tag head é a responsável por conter os metadados de um documento html, tais como titulo, descrição, data, autor, etc.


## Conteúdo
- [**Tag Link**](##tag-link)
- [**Open search**](#open-search)


### Tag Link
A tag link serve para linkar o documento atual ao que está sendo vinculado. Dentro dele há alguns atributos, que são:

`href` especifica o link do documento de referência a ser usado;
`rel` especificar a relação entre o documento atual e o vinculado;
`title` especifica o título do documento;
`type` espeçifica o tipo de documento.

### Open Search 
Essa tag tem como função, informar ao navegador qual o mecanismo de pesquisa usado pelo site e ao implementar, os navegadores permitirão, caso suportado, que os usuários façam pesquisa usando a barra de url do navegador
```
<link type="application/opensearchdescription+xml" rel="search"  title="Buscador" href="https://exemplo.com/opensearch.xml">
```

###### Referência
- [OpenSearch](https://www.algolia.com/doc/guides/building-search-ui/ui-and-ux-patterns/opensearch/js/){:target="_blank"}
